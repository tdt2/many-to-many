import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Collection;

public class Jdbc {
    public static Connection getConnection(){
        Connection c=null;
        try{
            String url="jdbc:mysql://localhost:3306/many to many";
            String user="root";
            String pw="root";
            c= DriverManager.getConnection(url,user,pw);
        }
        catch (Exception e){e.printStackTrace();}
        return c;
    }
    public static void closeConnection(Connection c){
        try{
            if(c!=null){c.close();}}
        catch (Exception e){e.printStackTrace();}
        }
    }

