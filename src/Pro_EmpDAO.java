import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Pro_EmpDAO implements DAO<Project_Employee> {
    public static Pro_EmpDAO getInstance(){
        return new Pro_EmpDAO();
    }
    @Override
    public int insert(Project_Employee pro_emp) {
        try{ String sql="insert into project_employee(id,projectid,employeeid)"+
                " values ("+pro_emp.getId()+" , "+pro_emp.getProjectid()+" , " + pro_emp.getEmployeeid() + ")";
            Connection c=Jdbc.getConnection();
            Statement sttm=c.createStatement();
            sttm.executeUpdate(sql);
            Jdbc.closeConnection(c);
        }
        catch(Exception e){e.printStackTrace();}
        return 0;
    }

    @Override
    public int update(Project_Employee project_employee) {
        return 0;
    }

    @Override
    public int delete(Project_Employee project_employee) {
        return 0;
    }

    @Override
    public ArrayList<Project_Employee> selectAll() {
        ArrayList<Project_Employee> list=new ArrayList<>();
        try {
            String sql = "select * from project_employee";
            Connection c = Jdbc.getConnection();
            Statement sttm = c.createStatement();
            ResultSet rs = sttm.executeQuery(sql);

            while (rs.next()){
                Project_Employee p=new Project_Employee();
                p.setId(rs.getInt("id"));
                p.setProjectid(rs.getInt("projectid"));
                p.setEmployeeid(rs.getInt("employeeid"));
                list.add(p);

            }
            Jdbc.closeConnection(c);
        }
        catch (Exception e){e.printStackTrace();}
        return list;
    }

    @Override
    public ArrayList<Project_Employee> selectBy() {
        return null;
    }


    public ArrayList<Project_Employee> selectBy(String Condition) {
        ArrayList<Project_Employee> list=new ArrayList<>();
        try {
            String sql = "select * from project_employee where "+Condition;
            Connection c = Jdbc.getConnection();
            Statement sttm = c.createStatement();
            ResultSet rs = sttm.executeQuery(sql);

            while (rs.next()){
                Project_Employee pe=new Project_Employee();
                pe.setId(rs.getInt("id"));
                pe.setEmployeeid(rs.getInt("employeeid"));
                pe.setProjectid(rs.getInt("projectid"));

                list.add(pe);

            }
            Jdbc.closeConnection(c);
        }
        catch (Exception e){e.printStackTrace();}


        return list;
    }



    @Override
    public boolean clear() {
        try {
            String sql = "delete from project_employee";
            Connection c = Jdbc.getConnection();
            Statement sttm = c.createStatement();
            sttm.executeUpdate(sql);
            Jdbc.closeConnection(c);
        }
        catch (Exception e){e.printStackTrace();}
        return true;

    }
    public ArrayList<Project_Employee> select(){
        ArrayList<Project_Employee> list=new ArrayList<>();
        try {
            String sql = "select employeeid from project_employee group by employeeid having count(projectid)> 1 ";
            Connection c = Jdbc.getConnection();
            Statement sttm = c.createStatement();
            ResultSet rs = sttm.executeQuery(sql);
            while (rs.next()) {
                Project_Employee pe=new Project_Employee();
                pe.setEmployeeid(rs.getInt("employeeid"));
//                pe.setProjectid(rs.getInt("projectid"));
                list.add(pe);
            }
            Jdbc.closeConnection(c);
        }
            catch(Exception e){e.printStackTrace();}

        return list;
    }
    }

