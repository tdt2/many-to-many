public class Project_Employee {
    private int id;

    public Project_Employee(int id, int projectid, int employeeid) {
        this.id = id;
        this.projectid = projectid;
        this.employeeid = employeeid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int projectid;
    private int employeeid;
    private Project project;
    private Employee employee;

    public Project_Employee(int projectid, int employeeid) {
        this.projectid = projectid;
        this.employeeid = employeeid;
    }

    public int getProjectid() {
        return projectid;
    }

    @Override
    public String toString() {
        return "Project_Employee{" +
                "projectid=" + projectid +
                ", employeeid=" + employeeid +
                ", project=" + project +
                ", employee=" + employee +
                '}';
    }

    public void setProjectid(int projectid) {
        this.projectid = projectid;
    }

    public int getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(int employeeid) {
        this.employeeid = employeeid;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Project_Employee(Employee employee) {
        this.employee = employee;
    }

    public Project_Employee(Project project) {
        this.project = project;
    }

    public Project_Employee() {
    }
}