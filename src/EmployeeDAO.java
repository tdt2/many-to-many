import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class EmployeeDAO implements DAO<Employee> {
    public static EmployeeDAO getInstance(){
        return new EmployeeDAO();}

    @Override
    public int insert(Employee employee) {
        try{ String sql="insert into employee(id,name,age,salary)"+
                " values ("+employee.getIdE()+" , '"+employee.getName()+"', "+employee.getAge()+
               " , "+employee.getSalary()+ ")";
            Connection c=Jdbc.getConnection();
            Statement sttm=c.createStatement();
            sttm.executeUpdate(sql);
            Jdbc.closeConnection(c);
        }
        catch(Exception e){e.printStackTrace();}
        return 0;
    }

    @Override
    public int update(Employee employee) {
        try{ String sql="Update employee Set" +
                " Name= '" +employee.getName() + "' , " +
                " age= " + employee.getAge() + " , " +
                " salary= " +employee.getSalary()+
                " where id= " + employee.getIdE();
            Connection c=Jdbc.getConnection();
            Statement sttm=c.createStatement();
            sttm.executeUpdate(sql);
            Jdbc.closeConnection(c);
        }
        catch(Exception e){e.printStackTrace();}
        return 0;
    }

    @Override
    public int delete(Employee employee) {
        try{ String sql="Delete from employee where"+" id="+employee.getIdE();
            Connection c=Jdbc.getConnection();
            Statement sttm=c.createStatement();
            sttm.executeUpdate(sql);
            Jdbc.closeConnection(c);
        }
        catch(Exception e){e.printStackTrace();}
        return 0;
    }

    @Override
    public ArrayList<Employee> selectAll() {
        ArrayList<Employee> list=new ArrayList<>();
        try{
            String sql="select * from employee";
            Connection c=Jdbc.getConnection();
            Statement sttm=c.createStatement();
            ResultSet rs= sttm.executeQuery(sql);
            while(rs.next()){
                Employee e=new Employee();
                e.setIdE(rs.getInt("id"));
                e.setName(rs.getString("name"));
                e.setAge(rs.getInt("age"));
                e.setSalary(rs.getInt("salary"));
                list.add(e);
            }

        }
        catch (Exception e){e.printStackTrace();}
        return list;
    }

    @Override
    public ArrayList<Employee> selectBy() {
        return null;
    }


    public ArrayList<Employee> selectBy(String Condition) {
        ArrayList<Employee> list=new ArrayList<>();
        try{
            String sql="select * from employee where "+Condition;
            Connection c=Jdbc.getConnection();
            Statement sttm=c.createStatement();
            ResultSet rs= sttm.executeQuery(sql);
            while(rs.next()){
                Employee e=new Employee();
                e.setIdE(rs.getInt("id"));
                e.setName(rs.getString("name"));
                e.setAge(rs.getInt("age"));
                e.setSalary(rs.getInt("salary"));
                list.add(e);
            }

        }
        catch (Exception e){e.printStackTrace();}
        return list;
    }

    @Override
    public boolean clear() {
        try {
            String sql = "delete from employee";
            Connection c = Jdbc.getConnection();
            Statement sttm = c.createStatement();
            sttm.executeUpdate(sql);
            Jdbc.closeConnection(c);
        }
        catch (Exception e){e.printStackTrace();}

        return true;
    }
}
