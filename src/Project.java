import java.util.ArrayList;
import java.util.Set;

public class Project {
    private int id;
    private String name;
    private int cash;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public Project(int id, String name, int cash) {
        this.id = id;
        this.name = name;
        this.cash = cash;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cash=" + cash +
                '}';
    }

    public Project() {
    }
}
