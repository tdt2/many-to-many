import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Set;

public class Main {
    Scanner sc = new Scanner(System.in);

    public static void createProject() {
        try {
            Project p = new Project();
            Scanner sc = new Scanner(System.in);
            System.out.println("Nhạp ID project:");
            p.setId(sc.nextInt());
            sc.nextLine();
            System.out.println("Nhap ten project:");
            p.setName(sc.nextLine());
            System.out.println("Nhap so tien project:");
            p.setCash(sc.nextInt());
            ProjectDAO.getInstance().insert(p);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static void creatEmployee() {
        try {
            Employee e = new Employee();
            Scanner sc = new Scanner(System.in);
            System.out.println("Nhap ID nv:");
            e.setIdE(sc.nextInt());
            System.out.println("Nhap ten nv:");
            sc.nextLine();
            e.setName(sc.nextLine());
            System.out.println("Nhap tuoi nv:");
            e.setAge(sc.nextInt());
            System.out.println("nhap luong nv:");
            e.setSalary(sc.nextInt());
            EmployeeDAO.getInstance().insert(e);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static void createPE() {
        try {
            Project_Employee pe = new Project_Employee();
            Scanner sc = new Scanner(System.in);
            System.out.println("Nhap STT:");
            pe.setId(sc.nextInt());
            System.out.println("Nhap ma nv:");
            pe.setEmployeeid(sc.nextInt());
            System.out.println("Nhap ma project:");
            pe.setProjectid(sc.nextInt());
            Pro_EmpDAO.getInstance().insert(pe);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static void clear() {
        EmployeeDAO.getInstance().clear();
        ProjectDAO.getInstance().clear();
        Pro_EmpDAO.getInstance().clear();
    }

    public static void showProject() {
        ArrayList<Project> list = ProjectDAO.getInstance().selectAll();
        list.forEach((Project) -> System.out.println(Project));
    }

    public static void showEmployee() {
        ArrayList<Employee> list = EmployeeDAO.getInstance().selectAll();
        list.forEach((Employee) -> System.out.println(Employee));
    }

    public static void showPE() {
        ArrayList<Project_Employee> listPE = Pro_EmpDAO.getInstance().selectAll();
        for (int i = 0; i < listPE.size(); i++) {
            Project_Employee pe = listPE.get(i);
            ArrayList<Project> listP = ProjectDAO.getInstance().selectBy("id= " + pe.getProjectid());
            ArrayList<Employee> listE = EmployeeDAO.getInstance().selectBy("id= " + pe.getEmployeeid());
            pe.setProject(listP.get(0));
            pe.setEmployee(listE.get(0));

            System.out.println(pe);

        }
    }

    public static void showEMP() {
        ArrayList<Project_Employee> list = Pro_EmpDAO.getInstance().select();
        list.forEach((Project_Employee)-> System.out.println(Project_Employee));



    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        boolean flag = true;
        int a;

        do {
            System.out.println("Nhan 1 de tao Project.");
            System.out.println("Nhan 2 de tao Employee.");
            System.out.println("Nhan 3 de tao PE.");
            System.out.println("Nhan 4 de hien thong tin Project.");
            System.out.println("Nhan 5 de hien thong tin Employee.");
            System.out.println("Nhan 6 de hien thong tin P-E.");
            System.out.println("Nhan 7 de Search nhung thanh nien lam nhieu Project.");
            System.out.println("Nhan 9 de xoa data.");
            a = sc.nextInt();

            switch (a) {
                case 1:
                    System.out.println("Nhap thong tin project:");
                    createProject();
                    break;
                case 2:
                    System.out.println("Nhap thong tin employee:");
                    creatEmployee();
                    break;
                case 3:
                    System.out.println("Nhap thong tin P-E:");
                    createPE();
                    break;
                case 4:
                    showProject();
                    break;
                case 5:
                    showEmployee();
                    break;
                case 6:
                    showPE();
                    break;
                case 7:
                    showEMP();
                    break;
                case 9:
                    System.out.println("Con cc gi dau ma xoa");
                    clear();
                    break;
                default:
                    System.out.println("Goodbye baby!");
                    flag = false;
                    break;
            }
        }
        while (flag);


    }


}

