import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.concurrent.locks.Condition;

public class ProjectDAO implements DAO<Project> {

    public static ProjectDAO getInstance(){
        return new ProjectDAO();
    }
    @Override
    public int insert(Project project) {
        try{ String sql="insert into project(id,name,cash)"+
                " values ("+project.getId()+" , '"+project.getName()+"', "+project.getCash()+")";
            Connection c=Jdbc.getConnection();
            Statement sttm=c.createStatement();
            sttm.executeUpdate(sql);
            Jdbc.closeConnection(c);
        }
        catch(Exception e){e.printStackTrace();}
        return 0;
    }

    @Override
    public int update(Project project) {
        try{ String sql= "Update project Set" +
                " Name= '" +project.getName() + "' , " +
                " cash= " + project.getCash() +
                " where id= " + project.getId();

            Connection c=Jdbc.getConnection();
            Statement sttm=c.createStatement();
            sttm.executeUpdate(sql);
            Jdbc.closeConnection(c);
        }
        catch(Exception e){e.printStackTrace();}
        return 0;
    }

    @Override
    public int delete(Project project) {
        try{ String sql="delete from project where id= "+project.getId();
            Connection c=Jdbc.getConnection();
            Statement sttm=c.createStatement();
            sttm.executeUpdate(sql);
            Jdbc.closeConnection(c);
        }
        catch(Exception e){e.printStackTrace();}
        return 0;
    }

    @Override
    public ArrayList<Project> selectAll() {
        ArrayList<Project> list=new ArrayList<>();
        try {
            String sql = "select * from project";
            Connection c = Jdbc.getConnection();
            Statement sttm = c.createStatement();
            ResultSet rs = sttm.executeQuery(sql);

            while (rs.next()){
                Project p=new Project();
                p.setId(rs.getInt("id"));
                p.setName(rs.getString("name"));
                p.setCash(rs.getInt("cash"));
                list.add(p);

            }
            Jdbc.closeConnection(c);
        }
        catch (Exception e){e.printStackTrace();}



        return list;
    }

    @Override
    public ArrayList<Project> selectBy() {
        return null;
    }


    public ArrayList<Project> selectBy(String Condition ) {
        ArrayList<Project> list=new ArrayList<>();
        try {
            String sql = "select * from project where " +Condition;
            Connection c = Jdbc.getConnection();
            Statement sttm = c.createStatement();
            ResultSet rs = sttm.executeQuery(sql);
            while (rs.next()){

                Project p=new Project();
                p.setId(rs.getInt("id"));
                p.setName(rs.getString("name"));
                p.setCash(rs.getInt("cash"));
                list.add(p);
            }
            Jdbc.closeConnection(c);
        }
        catch (Exception e){e.printStackTrace();}
        return list;
    }

    @Override
    public boolean clear() {
        try {
            String sql = "delete from project";
            Connection c = Jdbc.getConnection();
            Statement sttm = c.createStatement();
            sttm.executeUpdate(sql);
            Jdbc.closeConnection(c);
        }
        catch (Exception e){e.printStackTrace();}
        return true;
    }
}
